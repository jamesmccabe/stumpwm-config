;;; overrides.lisp --- preferred functionality for StumpWM

;; Copyright © 2020 James McCabe

;; Author: James McCabe <jamesmccabe07(at)gmail.com>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file changes some default behavior of StumpWM

;;; Code:

;;; Colors
;; overrides StumpWM default behavior of dimming normal colors
(defun update-color-map (screen)
  "Read *colors* and cache their pixel colors for use when rendering colored text."
  (labels ((map-colors (amt)
	     (loop for c in *colors*
		   as color = (lookup-color screen c)
		   do (adjust-color color amt)
		   collect (alloc-color screen color))))
    (setf (screen-color-map-normal screen) (apply #'vector (map-colors 0.00)))))

(update-color-map (current-screen))

;; fix colors in quit message
(defcommand quit-confirm () ()
  "Prompt the user to confirm quitting StumpWM."
  (if (y-or-n-p (format nil "~@{~a~^~%~}"
			"You are about to quit the window manager to TTY."
			"Really ^2quit^n ^4StumpWM^n?"
			"^5Confirm?^n "))
      (quit)
      (xlib:unmap-window (screen-message-window (current-screen)))))

;;; Typing in command input box
;; overrides hphen being added by space in colon command
(defcommand colon (&optional initial-input) (:rest)
  "Read a command from the user. @var{initial-text} is optional. When
supplied, the text will appear in the prompt.
String arguments with spaces may be passed to the command by
delimiting them with double quotes. A backslash can be used to escape
double quotes or backslashes inside the string. This does not apply to
commands taking :REST or :SHELL type arguments."
  (let ((*input-map* (copy-structure *input-map*)))
    (define-key *input-map* (kbd "SPC") 'input-insert-space)
    (define-key *input-map* (kbd "M-SPC") 'input-insert-space)
    (define-key *input-map* (kbd "RET") 'input-complete-and-submit)
    (let ((cmd (completing-read (current-screen)
				": "
				#'emacs-style-command-complete
				:initial-input (or initial-input ""))))
      (unless cmd
	(throw 'error :abort))
      (when (plusp (length cmd))
	(eval-command cmd t)))))


;;; Groups

;; I use a combination of the "%g" and "%n" in my modeline
;; For normal groups I use the "%g" display which shows all groups except hidden groups
;; When I toggle to the scratchpad it just shows that one group using "%n" in the modeline

(defvar *group-display-p* t
  "Variable to toggle modeline groups display")

(defcommand toggle-scratchpad () ()
  "Toggle scratchpad, move to hidden group or go back to the last active group"
  (if (not (eq (group-number (current-group)) -1))
      (progn
	(run-commands "gselect -1")
	(setf *group-display-p* nil))
      (progn
	(gother)
	(setf *group-display-p* t))))

;; add hook to set *group-display-p* to nil on group changes above 0 this stops
;; the scratchpad group view being used when cycling from that group to another
;; instead of using the toggle command
(defun ml-group-hook (arg1 arg2)
  (when (> (group-number (current-group)) 0)
      (when (not *group-display-p*)
	(setf *group-display-p* t))))

(add-hook *focus-group-hook* 'ml-group-hook)

;; modeline groups display
(add-screen-mode-line-formatter #\G 'ml-fmt-group-list)

(defun ml-fmt-group-list (ml)
  "Either displays all groups or just 1 hidden group/scratchpad"
  (if *group-display-p*
      (progn
	(format nil "~{~a~^ ~}" ; normal group modeline display
          (mapcar (lambda (g)
                    (let* ((str (format-expand *group-formatters* *group-format* g)))
                      (if (eq g (current-group))
                          (fmt-highlight str)
                          str)))
		  (non-hidden-groups (sort-groups (group-screen (mode-line-current-group ml)))))))
      (format nil "~a" (group-name (mode-line-current-group ml))))) ; scratchpad modeline group display))

;;; overrides.lisp ends here
